from django import forms

from .models import SessionKey


class SessionForm(forms.ModelForm):
    start_time = forms.DateTimeField(widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    end_time = forms.DateTimeField(widget=forms.widgets.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = SessionKey
        fields = ["start_time", "end_time"]


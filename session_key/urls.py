from django.urls import path
from . import views
from .middleware import auth_middleware, auth_middleware_id

urlpatterns = [
    path('session_generate/', auth_middleware(views.add_session), name="add_session"),
    path('sessions/', auth_middleware(views.get_sessions), name="sessions"),
    path('sessions/<int:id>/edit/', auth_middleware_id(views.edit_session), name="edit_session"),
    path("sessions/<int:id>/", auth_middleware_id(views.delete_session), name="delete_session"),
    path("", views.key, name="key"),
]
import uuid
from django.shortcuts import render, redirect
from .forms import SessionForm
from .models import SessionKey
from django.db.models import Q
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

# Create your views here.


class PublicId:
    @staticmethod
    def create_public_id():
        public_id = uuid.uuid4().int >> 75
        str_public_id = str(public_id)
        int_public_id = str_public_id[:8]
        return int(int_public_id)


def add_session(request):
    form = SessionForm(request.POST)
    context = {"form": form}
    if request.method == "POST":
        if form.is_valid():
            start_time = form.cleaned_data["start_time"]
            end_time = form.cleaned_data["end_time"]
            key = PublicId.create_public_id()
            data = SessionKey.objects.create(
                key=key,
                start_time=start_time,
                end_time=end_time,
            )
            data.save()
            return redirect("sessions")
    return render(request, "sessions/create_session.html", context)


def get_sessions(request):
    if request.method == "GET":
        order_by = "id"
        name = request.GET.get("search_value")
        query = Q()
        submit_button = request.GET.get("submit")
        page = request.GET.get("page")
        if name:
            query.add(
                Q(
                    Q(key__icontains=name)
                    | Q(start_time__icontains=name)
                    | Q(end_time__icontains=name)
                ),
                query.connector,
            )
        sessions = SessionKey.objects.filter(query).order_by(order_by)
        paginator = Paginator(sessions, 10)
        try:
            sessions = paginator.page(page)
        except PageNotAnInteger:
            sessions = paginator.page(1)
        except EmptyPage:
            sessions = paginator.page(paginator.num_pages)
        return render(
            request,
            "sessions/session_list.html",
            context={"sessions": sessions, "submit_button": submit_button},
        )


def edit_session(request, id):
    session = SessionKey.objects.get(id=id)
    form = SessionForm(
        initial={
            "start_time": session.start_time,
            "end_time": session.end_time
        }
    )
    if request.method == "POST":
        form = SessionForm(request.POST, instance=session)
        if form.is_valid():
            form.save()
            model = form.instance
            return redirect("sessions")
    return render(request, "sessions/update_session.html", context={"form": form})


def delete_session(request, id):
    session = SessionKey.objects.get(id=id)
    session.delete()
    return redirect("sessions")


def dashboard(request):
    return render(request, "sessions/dashboard.html")


def key(request):
    try:
        key = request.POST
        if key:
            int_key = int(key['key'])
            key_verification = SessionKey.objects.get(key=int_key)
            if timezone.now() <= key_verification.end_time and timezone.now() >= key_verification.start_time:
                if request.session.get('key'):
                    del request.session['key']
                request.session['key'] = int_key
                request.session.set_expiry(key_verification.end_time)
                return redirect("list_categories")
            else:
                messages.error(request, "your key is expired.")
                return redirect("/")
        return render(request, "sessions/otp.html")

    except ObjectDoesNotExist:
        messages.error(request, "invalid key.")
        return redirect("/")

    except KeyError:
        return redirect("/")

    except ValueError:
        messages.error(request, "please enter 8 digit numeric value.")
        return redirect("/")
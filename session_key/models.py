from django.db import models
from django.utils import timezone

# Create your models here.

class SessionKey(models.Model):
    class Meta:
        db_table = "session_key"

    key = models.BigIntegerField(editable=False, unique=True)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(null=True, blank=True)
from django.shortcuts import render
from django.db.models import Q
from .models import Category
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# Create your views here.


def get_categories(request):
    if request.method == "GET" and request.session.get('key'):
        order_by = "id"
        name = request.GET.get("search_value")
        query = Q()
        submit_button = request.GET.get("submit")
        page = request.GET.get("page")
        if name:
            query.add(
                Q(
                    Q(title__icontains=name)
                    | Q(description__icontains=name)
                    | Q(slug__icontains=name)
                    | Q(status__iexact=name)
                ),
                query.connector,
            )
        category = Category.objects.filter(query).order_by(order_by)
        paginator = Paginator(category, 10)
        try:
            category = paginator.page(page)
        except PageNotAnInteger:
            category = paginator.page(1)
        except EmptyPage:
            category = paginator.page(paginator.num_pages)
        return render(
            request,
            "category/index.html",
            context={"category": category, "submit_button": submit_button},
        )
    else:
        return render(
            request,
            "sessions/dashboard.html",
        )


def gm10dashboard(request):
    return render(request, "category/gm10-detail.html")


def film_role_dashboard(request):
    return render(request, "category/filmroll-detail.html")


def housemate_dashboard(request):
    return render(request, "category/housemate-detail.html")


def social_dashboard(request):
    return render(request, "category/category-list.html")
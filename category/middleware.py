from django.shortcuts import redirect


def auth_middleware(get_response):
    def middleware(request):
        if not request.session.get("key"):
            return redirect("/")
        response = get_response(request)
        return response

    return middleware


def auth_middleware_id(get_response):
    def middleware(request, id):
        if not request.session.get("key"):
            return redirect("/")
        response = get_response(request, id)
        return response

    return middleware
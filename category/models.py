from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.


class Category(models.Model):

    class Meta:
        db_table = "category"

    title = models.CharField(max_length=255)
    description = RichTextField()
    slug = models.SlugField(max_length=200)
    status = models.CharField(
        max_length=15,
        choices=([("active", "active"), ("inactive", "inactive")]),
        default="active",
    )

    def __str__(self):
        return self.title

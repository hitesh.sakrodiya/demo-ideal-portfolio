from django import forms

from ckeditor.widgets import CKEditorWidget

from .models import Category


class CategoryForm(forms.ModelForm):
    status = (("active", "active"), ("inactive", "inactive"))
    title = forms.CharField(required=False, max_length=255)
    slug = forms.SlugField(required=False, max_length=200)
    description = forms.CharField(widget=CKEditorWidget())
    status = forms.ChoiceField(choices=status, required=False)

    class Meta:
        model = Category
        fields = ["title", "description", "slug", "status"]



from django.urls import path
from . import views
from .middleware import auth_middleware

urlpatterns = [
    path("categories/", auth_middleware(views.get_categories), name="list_categories"),
    path("categories/gm10/", auth_middleware(views.gm10dashboard), name="gm10"),
    path("categories/housemate/", auth_middleware(views.housemate_dashboard), name="housemate"),
    path("categories/filmrole/", auth_middleware(views.film_role_dashboard), name="film_role"),
    path("categories/social/", auth_middleware(views.social_dashboard), name="social_media"),

]
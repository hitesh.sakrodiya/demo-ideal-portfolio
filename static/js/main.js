"use strict";

// AOS animation initialization
AOS.init();

// Live traders carousel
$(window).on('load', function () {
    $(".live-traders-carousel").owlCarousel({
      loop: true,
      margin: 40,
      nav: true,
      dots: false,
      lazyLoad: true,
      navSpeed: 700,
      navText: ["<", ""],
      responsive: {
        0: {
          items: 1.8,
        },
        400: {
          items: 2.5,
        },
        740: {
          items: 4,
        },
        940: {
          items: 5.7,
        },
      },
    });
});
